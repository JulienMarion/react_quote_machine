import React from 'react';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			quote: 'Quality means doing it right when no one is looking.',
			author: 'Henry Ford'
		};
		this.nextQuote = this.nextQuote.bind(this);
		this.randomQuote = this.randomQuote.bind(this);
	}

	nextQuote(content, name) {
		this.setState({
			quote: content,
			author: name
		});
	}

	async randomQuote() {
		const response = await fetch('https://api.quotable.io/random');
		const data = await response.json();
		this.nextQuote(data.content, data.author);
	}

	render() {
		return (
			<div id="quote-box">
				<q id="text">{this.state.quote}</q>
				<span id="author">{this.state.author}</span>
				<button id="new-quote" onClick={this.randomQuote}>
					Next &gt;&gt;
				</button>
			</div>
		);
	}
}

export default App;
// credits to lukePeavey for "https://api.quotable.io/random" on https://github.com/lukePeavey/quotable;
